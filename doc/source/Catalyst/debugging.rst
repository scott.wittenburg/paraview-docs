Debugging Tips
==============

* Set the environmental variable `PARAVIEW_LOG_CATALYST_VERBOSITY` to `INFO` to increase the verbosity of messages at the ParaViewCatalyst level.
* Dump the conduit nodes passed to Catalyst for inspection using `Catalyst Replay`_,
* Use `Catalyst Player`_ to iterate through times-series files from disk to emulate a simulation.

.. _Catalyst Replay: https://catalyst-in-situ.readthedocs.io/en/latest/catalyst_replay.html
.. _Catalyst Player: https://gitlab.kitware.com/paraview/catalyst-player
